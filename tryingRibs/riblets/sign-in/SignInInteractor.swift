//
//  SignInInteractor.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs
import RxSwift


public protocol SignInRouting: ViewableRouting {
    // TODO: Declare methods the interactor can invoke to manage sub-tree via the router.
    func detach(completion: (()->Void)?)
}

public protocol SignInPresentable: Presentable {
    weak var listener: SignInPresentableListener? { get set }
    // TODO: Declare methods the interactor can invoke the presenter to present data.
}

public protocol SignInListener: class {
    // TODO: Declare methods the interactor can invoke to communicate with other RIBs.
    func closeSignIn()
}

open class SignInInteractor: PresentableInteractor<SignInPresentable>, SignInInteractable, SignInPresentableListener {
    
    public weak var router: SignInRouting?
    public weak var listener: SignInListener?
    

    // TODO: Add additional dependencies to constructor. Do not perform any logic
    // in constructor.
    public override init(presenter: SignInPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    open override func didBecomeActive() {
        super.didBecomeActive()
        // TODO: Implement business logic here.
    }

    open override func willResignActive() {
        super.willResignActive()
        // TODO: Pause any business logic.
    }
    
    open func didCloseTap() {
        self.listener?.closeSignIn()
    }
    
    open func didLoad() {
        print("sign in loaded...")
    }
}
