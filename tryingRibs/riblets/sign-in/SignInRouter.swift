//
//  SignInRouter.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs


public protocol SignInInteractable:
    Interactable
{
    weak var router: SignInRouting? { get set }
    weak var listener: SignInListener? { get set }
}

public protocol SignInViewControllable: ViewControllable {
    // TODO: Declare methods the router invokes to manipulate the view hierarchy.
    func dismiss(viewController: ViewControllable, completion: (()->Void)?)
}

open class SignInRouter: ViewableRouter<SignInInteractable, SignInViewControllable>, SignInRouting {
    public var currentChild: Routing?

    // TODO: Constructor inject child builder protocols to allow building children.
    public override init(
        interactor: SignInInteractable,
        viewController: SignInViewControllable
    ) {
        super.init(
            interactor: interactor,
            viewController: viewController
        )
        interactor.router = self
    }
    
    private func detachCurrentChild(completion: (()->Void)?) {
        if let currentRoutingChild = self.currentChild {
            self.detachChild(currentRoutingChild)
            if let currentViewableRoutingChild = currentRoutingChild as? ViewableRouting {
                self.viewController.dismiss(
                    viewController: currentViewableRoutingChild.viewControllable,
                    completion: completion
                )
            }
            else {
                completion?()
            }
        }
        else {
            completion?()
        }
    }
    
    public func detach(completion: (() -> Void)?) {
        self.detachCurrentChild(completion: completion)
    }
}
