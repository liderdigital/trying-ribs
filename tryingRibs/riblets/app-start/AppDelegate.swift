//
//  AppDelegate.swift
//  taxi
//
//  Created by Igor de Oliveira Sa on 7/17/16.
//  Copyright © 2016 Igor de Oliveira Sa. All rights reserved.
//

import UIKit
import RIBs

@UIApplicationMain
open class AppDelegate: UIResponder, UIApplicationDelegate {
    
    public var window: UIWindow?

    private var launchRouter: LaunchRouting?

    open func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        
        let component = AppComponent()
        let launchRouter = RootBuilder(dependency: component).build(
            application: application,
            launchOptions: launchOptions,
            appDelegate: self
        )
        self.launchRouter = launchRouter
        launchRouter.launchFromWindow(window)
        
        return true
    }
}



