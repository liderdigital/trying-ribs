//
//  HomeInteractor.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs
import RxSwift



public protocol HomeRouting: ViewableRouting {
    // TODO: Declare methods the interactor can invoke to manage sub-tree via the router.
    func routeToAuthentication()
    func detach(completion: (()->Void)?)
}

public protocol HomePresentable: Presentable {
    weak var listener: HomePresentableListener? { get set }
    // TODO: Declare methods the interactor can invoke the presenter to present data.
    //func present(error: Error)
}

public protocol HomeListener: class {
    // TODO: Declare methods the interactor can invoke to communicate with other RIBs.
}

open class HomeInteractor: PresentableInteractor<HomePresentable>, HomeInteractable, HomePresentableListener {
    public weak var router: HomeRouting?
    public weak var listener: HomeListener?

    // TODO: Add additional dependencies to constructor. Do not perform any logic
    // in constructor.
    public override init(presenter: HomePresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    open override func didBecomeActive() {
        super.didBecomeActive()
        // TODO: Implement business logic here.
    }

    open override func willResignActive() {
        super.willResignActive()
        // TODO: Pause any business logic.
    }
    
    
    open func didSignInTap() {
        self.router?.routeToAuthentication()
    }
    
    open func closeAuthentication() {
        self.router?.detach(
            completion: nil
        )
    }
}
