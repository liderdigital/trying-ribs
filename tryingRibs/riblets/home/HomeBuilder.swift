//
//  HomeBuilder.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs


public protocol HomeDependency: Dependency {
    // TODO: Declare the set of dependencies required by this RIB, but cannot be
    // created by this RIB.
    var appDelegate: AppDelegate {get}
}

final class HomeComponent: Component<HomeDependency> {

    // TODO: Declare 'fileprivate' dependencies that are only used by this RIB.
    var appDelegate: AppDelegate {
        return dependency.appDelegate
    }
    
    let homeViewController: HomeViewController
    
    init(dependency: HomeDependency, homeViewController: HomeViewController) {
        self.homeViewController = homeViewController
        super.init(dependency: dependency)
    }
}

// MARK: - Builder

protocol HomeBuildable: Buildable {
    func build(withListener listener: HomeListener) -> HomeRouting
}

open class HomeBuilder: Builder<HomeDependency>, HomeBuildable {

    public override init(dependency: HomeDependency) {
        super.init(dependency: dependency)
    }

    open func build(withListener listener: HomeListener) -> HomeRouting {
        let viewController = UIStoryboard(
            name: "Home",
            bundle: nil
        ).instantiateViewController(
            withIdentifier: "homeViewController"
        ) as! HomeViewController

        let component = HomeComponent(
            dependency: dependency,
            homeViewController: viewController
        )
        
        let interactor = HomeInteractor(
            presenter: viewController
        )
        interactor.listener = listener
        
        let authenticationBuilder = AuthenticationBuilder(
            dependency: component
        )
        
        return HomeRouter(
            interactor: interactor,
            viewController: viewController,
            authenticationBuilder: authenticationBuilder
        )
    }
}
