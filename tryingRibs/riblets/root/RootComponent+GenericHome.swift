//
//  RootComponent+Home.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import Foundation
import RIBs

/// The dependencies needed from the parent scope of Root to provide for the Home scope.
// TODO: Update RootDependency protocol to inherit this protocol.
protocol RootDependencyHome: Dependency {
    
    // TODO: Declare dependencies needed from the parent scope of Root to provide dependencies
    // for the Home scope.
}

extension RootComponent: HomeDependency {
}
